// 载入外挂
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    jsmin = require('gulp-jsmin');

// 合并脚本
gulp.task('scripts-concat', function () {
    return gulp.src(['src/juicer.js', 'src/main.js'])
        .pipe(concat('xtpl.js'))
        .pipe(gulp.dest('./build'))
        .pipe(jsmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./build'));
});

// 合并文件
gulp.task('default', ['scripts-concat']);