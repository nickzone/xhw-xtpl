(function () {

    function renderTpl() {
        /*
            ************* part1 提取数据 **********
         */

        // 使用xTpl 替换juicer

        var xTpl = juicer;
        juicer = undefined;

        // 全局数据对象

        var xData = {};
        window.xData = xData;

        // 获取数据源 结构 div>ul?>li?>span?

        var dataEle = document.getElementById('xData');
        var dataBlocks = dataEle.getElementsByTagName('ul');
        var blockItem, key, dataLists, listItem, dataItem, dataProps, propItem, prop, val;
        var i, j, k;

        // 遍历翔宇数据片段

        for (i = 0; i < dataBlocks.length; i++) {

            blockItem = dataBlocks[i];
            key = blockItem.getAttribute('data-key');
            xData[key] = [];
            dataLists = blockItem.getElementsByTagName('li');

            // 遍历单个翔宇数据片段数据集合

            for (j = 0; j < dataLists.length; j++) {

                dataItem = {};
                listItem = dataLists[j];
                dataProps = listItem.getElementsByTagName('span');

                // 遍历该数据属性列表

                for (k = 0; k < dataProps.length; k++) {

                    propItem = dataProps[k];
                    prop = propItem.getAttribute('data-prop');
                    val = propItem.innerText;
                    dataItem[prop] = val;

                }

                xData[key].push(dataItem);
            }
        }

        /*
            ************* part2 提取模板并渲染 **********
         */

        // 过滤翔宇模板集合

        var scripts = document.scripts;
        var xtpls = [];

        for (var i = 0; i < scripts.length; i++) {
            var script = scripts[i];

            if (script.type === "text/xtpl") {
                xtpls.push(script);
            }
        }

        // 遍历模板

        for (var j = 0; j < xtpls.length; j++) {

            var xtpl = xtpls[j];
            var tplText = xtpl.innerHTML;
            var tplHtml = xTpl(tplText, xData);

            // 渲染模板并替换

            replaceDomWitchHtml(xtpl, tplHtml);
        }


        /**
         * 使用html字符串替换节点
         * @param {node} dom 被替换节点
         * @param {string} html 替换 html 字符串
         */

        function replaceDomWitchHtml(dom, html) {

            var frag = document.createElement('div');
            var parent = dom.parentNode;
            var childs;

            frag.innerHTML = html;
            childs = frag.childNodes;

            for (var i = 0; i < childs.length; i++) {
                var item = childs[i].cloneNode(true);
                parent.insertBefore(item, dom);
            }

            parent.removeChild(dom);
        }
    }

    /*
        ************* dom ready 后执行渲染 :TODO **********
     */

    function onDocLoaded(fn, context) {
        var context = context || window;
        var doc = window.document;
        var modern = doc.addEventListener;
        var pre = modern ? '' : 'on';
        var add = modern ? 'addEventListener' : 'attachEvent';
        var rem = modern ? 'removeEventListener' : 'detachEvent';
        var type = modern ? 'DOMContentLoaded' : 'readystatechange';

        // 是否已经加载完

        if (doc.readyState == 'complete') {
            fn.call(context);
            return;
        }

        function execFn(e) {
            if (modern) {
                fn.call(context);
                doc[rem](pre + type, execFn, false);
            } else if (doc.readyState == 'complete') {
                fn.call(context);
                doc[rem](pre + type, execFn, false);
            }
        }

        // 添加文档监听
        doc[add](pre + type, execFn, false);
    }

    onDocLoaded(renderTpl);

})();
